package com.example.pem.viewModels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.pem.models.Event
import com.example.pem.models.Location
import com.example.pem.repositories.EventRepository
import com.example.pem.repositories.UserRepository
import java.util.*
import kotlin.collections.set

class CreateEventViewModel : ViewModel() {

    var eventName: MutableLiveData<String> = MutableLiveData()
    var eventDescription: MutableLiveData<String> = MutableLiveData()
    var eventLocation: MutableLiveData<Location> = MutableLiveData()
    var eventDate: MutableLiveData<Date> = MutableLiveData()

    fun createEvent() {
        val event = Event(title = eventName.value, description = eventDescription.value, image = null, time = eventDate.value, location = eventLocation.value)
        UserRepository.currentUser.value?.let {
            event.members[it.id] = true
            event.creatorID = it.id
        }

        EventRepository.createEvent(event)
    }

    init {
        eventDate.value = Date()
    }

    private fun checkInputs(name: String, description: String): Boolean{
        return name.isNotEmpty() && description.isNotEmpty()
    }
}