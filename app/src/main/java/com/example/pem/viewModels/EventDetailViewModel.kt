package com.example.pem.viewModels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.pem.models.Event
import com.example.pem.repositories.EventRepository
import com.example.pem.repositories.UserRepository

class EventDetailViewModel: ViewModel() {

    var eventId: String = ""

    var event: MutableLiveData<Event> = EventRepository.event

    fun joinEvent(event: Event) {
        UserRepository.currentUser.value?.let {
            EventRepository.joinEvent(event, it)
        }
    }

    fun leaveEvent(event: Event) {
        UserRepository.currentUser.value?.let {
            EventRepository.leaveEvent(event, it)
        }
    }

    fun startListener() {
        EventRepository.observeEvent(eventId)
    }

    fun removeListener() {
        EventRepository.removeEventListener(eventId)
    }
}