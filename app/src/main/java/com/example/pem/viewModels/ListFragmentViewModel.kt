package com.example.pem.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.pem.models.Event
import com.example.pem.repositories.EventRepository
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class ListFragmentViewModel: ViewModel() {

    private var database: DatabaseReference = FirebaseDatabase.getInstance().reference

    private val events: MutableLiveData<List<Event>> = EventRepository.events

    fun getEvents(): LiveData<List<Event>> {
        return events
    }
}