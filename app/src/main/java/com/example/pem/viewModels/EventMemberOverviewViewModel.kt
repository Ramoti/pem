package com.example.pem.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.pem.models.Event
import com.example.pem.models.User
import com.example.pem.repositories.EventRepository

class EventMemberOverviewViewModel : ViewModel() {

    //TODO observe(certainEventId), wie bekomme ich die Id in das ViewModel?
    private val event: MutableLiveData<Event> = EventRepository.event
//    private val users: MutableLiveData<List<User>> =
    //TODO alle Teilnehmer des jeweiligen Events mit allen User Infos

    fun getEventMembers(): LiveData<List<User>>? {  // ? entfernen
        return null
    }
}
