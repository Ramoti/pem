package com.example.pem.repositories

import android.graphics.Bitmap
import androidx.lifecycle.MutableLiveData
import com.example.pem.models.Event
import com.example.pem.models.User
import com.google.firebase.database.*

object EventRepository {

    private var database: DatabaseReference = FirebaseDatabase.getInstance().reference.child("events")

    var singleEventListener: ValueEventListener? = null

    val events: MutableLiveData<List<Event>> = MutableLiveData()

    val event: MutableLiveData<Event> = MutableLiveData()

    var eventImage: Bitmap? = null

    init {
        loadEvents()
    }

    fun loadEvents() {

        val eventList: MutableList<Event> = mutableListOf()

        val eventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                eventList.clear()
                try {
                    dataSnapshot.children.mapNotNullTo(eventList) {
                        it.getValue(Event::class.java)
                    }
                } catch (e: DatabaseException) {

                }

                events.value = eventList
            }

            override fun onCancelled(databaseError: DatabaseError) {
                println("loadPost:onCancelled ${databaseError.toException()}")
            }
        }

        database.addValueEventListener(eventListener)
    }

    fun addEvent(newEvent: Event) {
        val key = database.push().key
        key?.let {
            newEvent.id = key
            database.child(key).setValue(newEvent)
        }
    }

    fun joinEvent(event: Event, user: User) {
        if (event.id.isEmpty()) {
            return
        }
        val ref = database.child(event.id)
        ref.runTransaction(object : Transaction.Handler {
            override fun doTransaction(mutableData: MutableData): Transaction.Result {
                val p = mutableData.getValue(Event::class.java)
                    ?: return Transaction.success(mutableData)

                p.members[user.id] = true
                // Set value and report transaction success
                mutableData.value = p
                return Transaction.success(mutableData)
            }

            override fun onComplete(
                databaseError: DatabaseError?,
                b: Boolean,
                dataSnapshot: DataSnapshot?
            ) {
                // Transaction completed
            }
        })
    }

    fun leaveEvent(event: Event, user: User) {
        if (event.id.isEmpty()) {
            return
        }
        val ref = database.child(event.id)
        ref.runTransaction(object : Transaction.Handler {
            override fun doTransaction(mutableData: MutableData): Transaction.Result {
                val p = mutableData.getValue(Event::class.java)
                    ?: return Transaction.success(mutableData)

                p.members.remove(user.id)
                // Set value and report transaction success
                mutableData.value = p
                return Transaction.success(mutableData)
            }

            override fun onComplete(
                databaseError: DatabaseError?,
                b: Boolean,
                dataSnapshot: DataSnapshot?
            ) {
                // Transaction completed
            }
        })
    }

    fun createEvent(event: Event) {
        val key = database.push().key

        key?.let {id ->
            event.id = id
            database.child(id).setValue(event)
            eventImage?.let { image ->
                ImageRepository.uploadEventImage(image, id)
            }
        }
    }

    fun observeEvent(eventId: String) {

        val eventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                var newValue = dataSnapshot.getValue(Event::class.java)

                newValue?.let {
                    event.value = newValue
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                println("loadPost:onCancelled ${databaseError.toException()}")
            }
        }

        database.child(eventId).addValueEventListener(eventListener)
        singleEventListener = eventListener
    }

    fun removeEventListener(eventId: String) {
        singleEventListener?.let {
            database.child(eventId).removeEventListener(it)
        }
    }

    fun updateEvent(event: Event) {
        database.child(event.id).setValue(event)
    }
}