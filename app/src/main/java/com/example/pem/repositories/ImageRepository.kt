package com.example.pem.repositories

import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import android.util.Log
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import java.io.ByteArrayOutputStream
import java.io.IOException

object ImageRepository {
    private var storage = FirebaseStorage.getInstance()
    private var storageRef = storage.reference
    private var eventImageRef: StorageReference? = storageRef.child("events")
    private var userImageRef: StorageReference? = storageRef.child("user")



    // Download


    /**
     * pass the view which is supposed to display the image
     */
    fun loadUserImage(userId: String, view: ImageView, context: Context) {
        getUserRef(userId)?.let {
            loadImage(it, context, view)
        }
    }

    /**
     * pass the view which is supposed to display the image
     */
    fun loadEventImage(eventId: String, view: ImageView, context: Context) {
        getEventRef(eventId)?.let {
            loadImage(it, context, view)
        }
    }

    private fun loadImage(reference: StorageReference, context: Context, view: ImageView) {
        try {
            Glide
                .with(context)
                .load(reference)
                .into(view)
        } catch (e: IOException) {
            Log.e("ImageRepository", "Could not open image for " + reference.path)
        }
    }

    // Upload

    fun uploadEventImage(filePath: Uri, eventId: String) {
        getEventRef(eventId)?.let {
            uploadImageByUri(it, filePath)
        }
    }

    fun uploadEventImage(image: Bitmap, eventId: String) {
        getEventRef(eventId)?.let {
            uploadImageByBitmap(it, image)
        }
    }

    fun uploadUserImage(filePath: Uri, userId: String) {
        getUserRef(userId)?.let {
            uploadImageByUri(it, filePath)
        }
    }

    fun uploadUserImage(image: Bitmap, userId: String) {
        getUserRef(userId)?.let {
            uploadImageByBitmap(it, image)
        }
    }

    private fun uploadImageByUri(reference: StorageReference, filePath: Uri) {
        reference.putFile(filePath)
            .addOnSuccessListener {
                Log.i("ImageRepository", "uploadImageByUri: Image successfully uploaded")

            }
            .addOnFailureListener {
                Log.e("ImageRepository", "uploadImageByUri: Image upload failed")
            }
    }

    private fun uploadImageByBitmap(reference: StorageReference, image: Bitmap) {
        val baos = ByteArrayOutputStream()
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val data = baos.toByteArray()

        reference.putBytes(data)
            .addOnSuccessListener {
                Log.i("ImageRepository", "uploadImageByBitmap: Image successfully uploaded")
            }
            .addOnFailureListener {
                Log.e("ImageRepository_Bitmap", "uploadImageByBitmap: Image upload failed")
            }
    }

    private fun getUserRef(userId: String): StorageReference? {
        return userImageRef?.let { ref -> ref.child(userId) }
    }

    private fun getEventRef(eventId: String): StorageReference? {
        return eventImageRef?.let { ref -> ref.child(eventId) }
    }

}