package com.example.pem.repositories

import androidx.lifecycle.MutableLiveData
import com.example.pem.models.User
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*

object UserRepository {

    private var database: DatabaseReference = FirebaseDatabase.getInstance().reference.child("users")


    var currentUser: MutableLiveData<User> = MutableLiveData()

    private var userListener: ValueEventListener? = null

    fun loadUser(userId: String) {
        userListener?.let {
            database.child(userId).removeEventListener(it)
        }

        userListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                var user = dataSnapshot.getValue(User::class.java)

                user?.let {
                    currentUser?.value = user
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                println("loadPost:onCancelled ${databaseError.toException()}")
            }
        }

        userListener?.let {
            database.child(userId).addValueEventListener(it)
        }
    }

    fun createUser(user: FirebaseUser) {
        val newUser = User()
        newUser.id = user.uid
        user.email?.let {
            newUser.email = it
        }

        database.child(user.uid).setValue(newUser)
        loadUser(newUser.id)
    }

    fun updateUser(user: User) {
        database.child(user.id).setValue(user)
    }
}