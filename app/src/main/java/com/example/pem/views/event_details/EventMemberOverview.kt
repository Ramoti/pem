package com.example.pem.views.event_details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.pem.R
import com.example.pem.models.User
import com.example.pem.viewModels.EventMemberOverviewViewModel


class EventMemberOverview : Fragment() {

    companion object {
        fun newInstance() = EventMemberOverview()
    }

    private lateinit var viewModel: EventMemberOverviewViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.event_member_overview_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(EventMemberOverviewViewModel::class.java)
        viewModel.getEventMembers()?.let { memberList -> memberList.observe(this, Observer<List<User>>{member ->
            //TODO display members
        }) }
    }

}
