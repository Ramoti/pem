package com.example.pem.views.event_create

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.pem.R
import com.example.pem.databinding.FragmentEditNameBinding
import com.example.pem.interfaces.CreateEventButtonClickListener
import com.example.pem.repositories.UserRepository
import com.example.pem.viewModels.CreateEventViewModel
import kotlinx.android.synthetic.main.fragment_create_event_summary.*
import kotlinx.android.synthetic.main.fragment_edit_name.*

class EditNameAndDescriptionFragment(private val createEventViewModel: CreateEventViewModel, private val createEventButtonClickListener: CreateEventButtonClickListener) : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var binding : FragmentEditNameBinding = DataBindingUtil.inflate(inflater ,
            R.layout.fragment_edit_name,container , false)
        var view : View  = binding.root
        binding.model = createEventViewModel
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        editNameContinueButton.setOnClickListener {
            createEventButtonClickListener.onContinueClicked()
        }

        UserRepository.currentUser.value?.let { user->
            editNameTitle.text = "Hallo " + user.name
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        activity?.let { activity ->
            createEventViewModel.eventName.observe(activity, Observer<String> {name ->
                editNameContinueButton?.isEnabled = name.isNotEmpty() && !createEventViewModel.eventDescription.value.isNullOrEmpty()
            })

            createEventViewModel.eventDescription.observe(activity, Observer<String> {description ->
                editNameContinueButton?.isEnabled = description.isNotEmpty() && !createEventViewModel.eventName.value.isNullOrEmpty()
            })
        }

    }
}