package com.example.pem.views



import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.pem.R
import com.example.pem.repositories.UserRepository
import com.example.pem.views.event_overview.EventOverviewActivity
import com.google.firebase.auth.FirebaseAuth


class LoginActivity : AppCompatActivity() {
    //global variables
    private var email: String? = null
    private var password: String? = null

    //UI elements
    private var etEmail: EditText? = null
    private var etPassword: EditText? = null
    private var buttonNewUser: Button? = null
    private var buttonLogin: Button? = null
    //Firebase references
    private var mAuth: FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        initialise()
        buttonNewUser!!.setOnClickListener {newUser() }

        buttonLogin!!.setOnClickListener {
            checkUser()
        }

        mAuth?.let { mAuth ->
            if(mAuth.currentUser != null) loggedIn()
        }

    }

    private fun initialise() {
        etEmail = findViewById<View>(R.id.loginEmail) as EditText
        etPassword = findViewById<View>(R.id.loginPassword) as EditText
        buttonLogin = findViewById<View>(R.id.buttonLogin) as Button
        buttonNewUser = findViewById<View>(R.id.buttonNewUser) as Button
        mAuth = FirebaseAuth.getInstance()
    }


    //invoked after successful login
    fun loggedIn() {
        FirebaseAuth.getInstance().currentUser?.uid?.let {
            UserRepository.loadUser(it)
        }

        val intent = Intent(this, EventOverviewActivity::class.java)
        startActivity(intent)
        finish()
    }

    fun checkUser()  {
        email = etEmail?.text.toString()
        password = etPassword?.text.toString()
        if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(password)) {
            mAuth!!.signInWithEmailAndPassword(email!!, password!!)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        Toast.makeText(this@LoginActivity, "Correct!Here we go!",
                            Toast.LENGTH_SHORT).show()
                        loggedIn()
                    } else {
                        Toast.makeText(this@LoginActivity, "Wrong Email or Password",
                            Toast.LENGTH_SHORT).show()
                    }
                }
        } else {
            Toast.makeText(this, "Enter all details", Toast.LENGTH_SHORT).show()
        }
    }

    fun newUser(){
        email = etEmail?.text.toString()
        password = etPassword?.text.toString()
        if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(password)) {
            mAuth!!
                .createUserWithEmailAndPassword(email!!, password!!)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        verifyEmail()
                        Toast.makeText(this@LoginActivity, "Registration: yes.",
                            Toast.LENGTH_SHORT).show()
                        FirebaseAuth.getInstance().currentUser?.let {
                            UserRepository.createUser(it)
                            val intent = Intent(this, AddNewInfoUser::class.java)
                            startActivity(intent)
                        }

                    } else {
                        // If sign in fails, display a message to the user.
                        Toast.makeText(this@LoginActivity, "Authentication failed.",
                            Toast.LENGTH_SHORT).show()
                    }
                }

        }else{
            Toast.makeText(this@LoginActivity, "Registration failed",
                Toast.LENGTH_SHORT).show()

        }
    }
    private fun verifyEmail() {
        val mUser = mAuth!!.currentUser;
        mUser!!.sendEmailVerification()
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                } else {
                    Toast.makeText(this@LoginActivity,
                        "Failed to send verification email.",
                        Toast.LENGTH_SHORT).show()
                }
            }
    }
}





