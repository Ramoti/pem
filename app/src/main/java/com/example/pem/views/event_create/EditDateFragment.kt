package com.example.pem.views.event_create

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.example.pem.R
import com.example.pem.interfaces.CreateEventButtonClickListener
import com.example.pem.repositories.UserRepository
import com.example.pem.viewModels.CreateEventViewModel
import kotlinx.android.synthetic.main.fragment_create_event_summary.backButton
import kotlinx.android.synthetic.main.fragment_edit_date.*
import java.util.*

class EditDateFragment(private val createEventViewModel: CreateEventViewModel, private val createEventButtonClickListener: CreateEventButtonClickListener) : Fragment(), DatePickerDialog.OnDateSetListener {

    private val hasSetDate = MutableLiveData<Boolean>(false)
    private val hasSetTime = MutableLiveData<Boolean>(false)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_edit_date, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        enterDateButton.setOnClickListener{
            showDatePicker()
        }

        enterTimeButton.setOnClickListener {
            showTimePicker()
        }

        backButton.setOnClickListener {
            createEventButtonClickListener.onBackClicked()
        }

        editDateContinueButton.setOnClickListener {
            createEventButtonClickListener.onContinueClicked()
        }

        UserRepository.currentUser.value?.let {user ->
            editDateTitle.text = "Hallo " + user.name
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        activity?.let {activity ->
            hasSetDate.observe(activity, Observer<Boolean> {hasSetDate ->
                editDateContinueButton.isEnabled = hasSetDate && hasSetTime.value!!
            })

            hasSetTime.observe(activity, Observer<Boolean> {hasSetTime ->
                editDateContinueButton.isEnabled = hasSetTime && hasSetDate.value!!
            })
        }

    }

    private fun showDatePicker() {
        val dataPickerDialog = DatePickerDialog(
            context!!,
            this,
            Calendar.getInstance().get(Calendar.YEAR),
            Calendar.getInstance().get(Calendar.MONTH),
            Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
        )

        dataPickerDialog.show()
    }

    private fun showTimePicker() {
        val calendar = Calendar.getInstance()
        val timeListener = TimePickerDialog.OnTimeSetListener { _, hour, minute ->
            calendar.time = createEventViewModel.eventDate.value
            calendar.set(Calendar.HOUR_OF_DAY, hour)
            calendar.set(Calendar.MINUTE, minute)
            createEventViewModel.eventDate.value = calendar.time
            hasSetTime.value = true
        }
        val timePickerDialog = TimePickerDialog(
            context,
            timeListener,
            calendar.get(Calendar.HOUR_OF_DAY),
            calendar.get(Calendar.MINUTE),
            true
        )

        timePickerDialog.show()
    }

    override fun onDateSet(p0: DatePicker?, p1: Int, p2: Int, p3: Int) {
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.YEAR, p1)
        calendar.set(Calendar.MONTH, p2)
        calendar.set(Calendar.DAY_OF_MONTH, p3)
        createEventViewModel.eventDate.value = calendar.time
        hasSetDate.value = true
    }
}