package com.example.pem.views

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.example.pem.R
import com.example.pem.repositories.UserRepository
import com.example.pem.repositories.UserRepository.updateUser
import com.example.pem.views.event_overview.EventOverviewActivity
import com.google.firebase.auth.FirebaseAuth

class AddNewInfoUser:  AppCompatActivity() {



    //UI elements
    private var backButton: Button? = null
    private var saveButton: Button? = null
   // private var radio1Button: Button? = null
   // private var radio2Button: Button? = null
    private var accountName: EditText? = null

    private var handyNumber: EditText? = null
    //Firebase references
    private var mAuth: FirebaseAuth? = null
    var user =UserRepository.currentUser.value

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.user_details_1)
        saveButton = findViewById<View>(R.id.savebutton) as Button
        backButton = findViewById<View>(R.id.backbutton) as Button
        saveButton!!.setOnClickListener {initialise()

            UserRepository.currentUser.value?.let {
                //Save Parameters: gender,Handynumber,Accountname, Photo
                it.name= accountName?.text.toString()
                it.phone=handyNumber?.text.toString()
                updateUser(it)

            }
            val intent = Intent(this, EventOverviewActivity::class.java)
            startActivity(intent)
            finish()


            }


        backButton!!.setOnClickListener{
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }


    }
  fun initialise() {
        accountName = findViewById<View>(R.id.accountname) as EditText
        handyNumber = findViewById<View>(R.id.telephone) as EditText
       // radio1Button =findViewById<View>(R.id.radiowoman) as Button
      //  radio2Button =findViewById<View>(R.id.radioman) as Button
  }



}

