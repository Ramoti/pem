package com.example.pem.views.event_details

import android.Manifest
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Resources
import android.net.Uri
import android.os.Bundle
import android.provider.CalendarContract
import android.util.Log
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.content.getSystemService
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.pem.R
import com.example.pem.models.Event
import com.example.pem.repositories.ImageRepository
import com.example.pem.repositories.UserRepository
import com.example.pem.viewModels.EventDetailViewModel
import kotlinx.android.synthetic.main.activity_event_detail.*
import java.security.Permission
import java.text.SimpleDateFormat


class EventDetailActivity : AppCompatActivity() {
    private lateinit var event: Event

    private lateinit var viewModel: EventDetailViewModel

    private val dateFormat = SimpleDateFormat("dd-MM-yyyy\nHH:mm")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event_detail)
        event = intent.getParcelableExtra("event")

        viewModel = ViewModelProviders.of(this).get(EventDetailViewModel::class.java)
        viewModel.eventId = event.id

        viewModel.event.observe(this, Observer<Event> { newEvent ->
            event = newEvent
            UserRepository.currentUser.value?.let {
                if (event.members.containsKey(it.id)) {
                    eventDetailJoin.text = "Verlassen"
                    eventDetailJoinCard.setBackgroundColor(applicationContext.getColor(R.color.transparent))
                    eventDetailJoin.setTextColor(applicationContext.getColor(R.color.textDark))
                } else {
                    eventDetailJoin.text = "Mitmachen"
                    eventDetailJoinCard.setBackgroundColor(applicationContext.getColor(R.color.accentPrimary))
                    eventDetailJoinCard.radius = 9f
                    eventDetailJoin.setTextColor(applicationContext.getColor(R.color.textLight))
                }

                eventDetailTitle.text = event.title
                eventDetailDescription.text = event.description
                eventDetailPlace.text = event.location?.name
                eventDetailDate.text = dateFormat.format(event.time)

                eventMemberLayout.removeAllViews()

                for (x in 0 until event.members.size) {
                    val imageView = ImageView(this)
                    imageView.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.account_icon))
                    val pixel = 40 * Resources.getSystem().displayMetrics.density
                    val layoutParams = LinearLayout.LayoutParams(pixel.toInt(), pixel.toInt())
                    imageView.layoutParams = layoutParams
                    eventMemberLayout.addView(imageView)
                }
            }
        })

        eventDetailTitle.text = event.title
        eventDetailDescription.text = event.description
        eventDetailDate.text = dateFormat.format(event.time)
        eventDetailPlace.text = event.location?.name

        //displays event image at eventDetailImage imageView
        ImageRepository.loadEventImage(event.id, eventDetailImage, this)

        eventDetailJoin.setOnClickListener {
            Log.i("Mitmachen Button", "CLICKED")
            UserRepository.currentUser.value?.let {
                if (event.members.containsKey(it.id)) {
                    viewModel.leaveEvent(event)
                } else {
                    viewModel.joinEvent(event)
                    createCalendarEvent()
                }
            }
        }

        eventDetailPlace.setOnClickListener {
            val gmmIntentUri = Uri.parse("geo:${event.location?.lat},${event.location?.lng}")
            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            mapIntent.setPackage("com.google.android.apps.maps")
            if (mapIntent.resolveActivity(packageManager) != null) {
                startActivity(mapIntent)
            }
        }
    }

    fun createCalendarEvent() {
        event.time?.let {
            val values = ContentValues().apply {
                put(CalendarContract.Events.DTSTART, it.time)
                put(CalendarContract.Events.DTEND, it.time + 720000)
                put(CalendarContract.Events.TITLE, event.title)
                put(CalendarContract.Events.DESCRIPTION, event.description)
                put(CalendarContract.Events.CALENDAR_ID, 3)
                put(CalendarContract.Events.EVENT_TIMEZONE, "Europe/Berlin")
            }

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_CALENDAR) == PackageManager.PERMISSION_GRANTED) {
                    contentResolver.insert(CalendarContract.Events.CONTENT_URI, values).also {
                        it?.lastPathSegment!!.toLong().also {
                            val reminderValues = ContentValues().apply {
                                put(CalendarContract.Reminders.MINUTES, 120)
                                put(CalendarContract.Reminders.EVENT_ID, it)
                                put(CalendarContract.Reminders.METHOD, CalendarContract.Reminders.METHOD_ALERT)
                            }
                            contentResolver.insert(CalendarContract.Reminders.CONTENT_URI, reminderValues)
                        }
                    }
                }
        }
    }

    override fun onStart() {
        super.onStart()
        viewModel.startListener()
    }

    override fun onStop() {
        super.onStop()
        viewModel.removeListener()
    }


}
