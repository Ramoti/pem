package com.example.pem.views

import android.app.SearchManager
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.SearchView
import androidx.appcompat.app.AppCompatActivity
import com.example.pem.R
import com.example.pem.views.event_overview.list.ListFragment

class SearchActivity : AppCompatActivity(), SearchView.OnQueryTextListener{
        override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_search)
        Log.i("Search", "Activity")

        if (Intent.ACTION_SEARCH == intent.action) {
            Log.i("Search", "received SEARCH intent")
            intent.getStringExtra(SearchManager.QUERY)?.let { query ->
                performSearch(query)
            }
        }

    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        performSearch(query!!)
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        performSearch(newText!!)
        Log.i("Search", "newText: $newText")
        return true
    }

    fun performSearch(query: String) {
        Log.i("Search", "query: $query")
        ListFragment.filter.postValue(query)
    }
}
