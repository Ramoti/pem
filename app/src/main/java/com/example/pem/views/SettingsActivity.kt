package com.example.pem.views

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import com.example.pem.R
import com.google.firebase.auth.FirebaseAuth

class SettingsActivity : AppCompatActivity(), SignOut {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings_activity)




        supportFragmentManager
            .beginTransaction()
            .replace(R.id.settings, SettingsFragment(this))
            .commit()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


    }

    override fun doIt() {
        val intent = Intent(this, LoginActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivity(intent)
        finishAffinity()
    }


    class SettingsFragment(private val signOut: SignOut) : PreferenceFragmentCompat() {
        override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey)

            findPreference<Preference>("sign_out")?.setOnPreferenceClickListener {
                FirebaseAuth.getInstance().signOut()
                signOut.doIt()
                true
            }
        }
    }


}

interface SignOut {
    fun doIt()
}