package com.example.pem.views.event_overview.map

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.example.pem.R
import com.example.pem.models.Event
import com.example.pem.repositories.EventRepository
import com.example.pem.views.event_details.EventDetailActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions


class MapFragment(private val currentLocation: MutableLiveData<LatLng>) : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.map_event_overview, container, false)

        val mapFragment =
            childFragmentManager.findFragmentById(R.id.mapEventOverview) as SupportMapFragment?
        mapFragment!!.getMapAsync { mMap ->
            mMap.mapType = GoogleMap.MAP_TYPE_NORMAL

            mMap.clear()

            currentLocation.observe(this, Observer<LatLng> { location ->
                if(location != null) {
                    val googlePlex = CameraPosition.builder()
                        .target(location)
                        .zoom(10f)
                        .bearing(0f)
                        .tilt(45f)
                        .build()

                    mMap.animateCamera(CameraUpdateFactory.newCameraPosition(googlePlex), 1000, null)
                }
            })

            EventRepository.events.observe(this, Observer<List<Event>> { events ->
                events.forEach { event ->
                    event.location?.let { location ->
                        mMap.addMarker(
                            MarkerOptions()
//                                .position(LatLng(48.137154, 11.576124))
                                .position(LatLng(location.lat!!, location.lng!!))
                                .title(event.title)
//                                .icon(BitmapDescriptorFactory.fromBitmap(getBitmap(R.drawable.star_icon)))
                                .snippet(event.description))
                            .tag = event.id
                    }
                }
            })

            mMap.setOnInfoWindowClickListener { marker ->
                val intent = Intent(context, EventDetailActivity::class.java)
                EventRepository.events.observe(this, Observer { eventList ->
                    val event = eventList.find { event ->
                        event.id == marker.tag.toString()
                    }
                    intent.putExtra("event", event)
                    context?.startActivity(intent)
                })
            }


        }

        return rootView
    }

    private fun getBitmap(drawableRes: Int): Bitmap {
        val drawable = resources.getDrawable(drawableRes)
        val canvas = Canvas()
        val bitmap = Bitmap.createBitmap(drawable.intrinsicWidth, drawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        canvas.setBitmap(bitmap)
        drawable.setBounds(0, 0, drawable.intrinsicWidth, drawable.intrinsicHeight)
        drawable.draw(canvas)

        return bitmap
    }

    // TODO
    //      style card next to marker

}
