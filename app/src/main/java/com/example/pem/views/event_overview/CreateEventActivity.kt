package com.example.pem.views.event_overview

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.example.pem.R
import com.example.pem.adapters.CreateEventViewPagerAdapter
import com.example.pem.databinding.FragmentCreateEventBinding
import com.example.pem.interfaces.CreateEventButtonClickListener
import com.example.pem.viewModels.CreateEventViewModel
import kotlinx.android.synthetic.main.fragment_create_event.*
import android.app.Activity
import android.view.View
import android.view.inputmethod.InputMethodManager


class CreateEventActivity: AppCompatActivity(), CreateEventButtonClickListener {

    private lateinit var viewModel: CreateEventViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var binding: FragmentCreateEventBinding = DataBindingUtil.setContentView(this, R.layout.fragment_create_event)

        viewModel = ViewModelProviders.of(this).get(CreateEventViewModel::class.java)
        binding.model = viewModel

        createEventViewPager.adapter = CreateEventViewPagerAdapter(supportFragmentManager, viewModel, this)
    }

    override fun onContinueClicked() {
        createEventViewPager.currentItem += 1
        hideKeyboard()
    }

    override fun onBackClicked() {
        createEventViewPager.currentItem -= 1
        hideKeyboard()
    }

    override fun createEventClicked() {
        viewModel.createEvent()
        finish()
    }

    fun hideKeyboard() {
        val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(this)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}