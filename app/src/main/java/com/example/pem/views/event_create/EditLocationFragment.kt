package com.example.pem.views.event_create

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.pem.R
import com.example.pem.interfaces.CreateEventButtonClickListener
import com.example.pem.models.Location
import com.example.pem.repositories.UserRepository
import com.example.pem.viewModels.CreateEventViewModel
import com.google.android.gms.common.api.Status
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import kotlinx.android.synthetic.main.fragment_edit_location.*
import java.util.*

class EditLocationFragment(private var createEventViewModel: CreateEventViewModel, private var createEventButtonClickListener: CreateEventButtonClickListener) : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_edit_location, container, false)

        if (!Places.isInitialized()) {
            Places.initialize(context!!, "AIzaSyDHAZtdHVZwJqANlRTh_qEGTZAQe-cZtfg", Locale.GERMANY)
        }

        val autocompleteFragment = childFragmentManager.findFragmentById(R.id.autocomplete_fragment) as AutocompleteSupportFragment
        autocompleteFragment.setPlaceFields(listOf(Place.Field.LAT_LNG, Place.Field.NAME, Place.Field.ADDRESS))
        autocompleteFragment.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onPlaceSelected(place: Place) {
                locationSelectedAddressSubtitle.text = place.address
                createEventViewModel.eventLocation.value = Location(name = place.name, lat = place.latLng?.latitude, lng = place.latLng?.longitude, address = place.address)
            }

            override fun onError(status: Status) {

            }
        })

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        locationBackButton.setOnClickListener {
            createEventButtonClickListener.onBackClicked()
        }

        locationContinueButton.setOnClickListener {
            createEventButtonClickListener.onContinueClicked()
        }

        UserRepository.currentUser.value?.let { user ->
            locationTitle.text = "Hallo " + user.name
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        activity?.let { activity ->
            createEventViewModel.eventLocation.observe(activity, Observer<Location> {location ->
                locationContinueButton.isEnabled = location != null
            })
        }
    }
}