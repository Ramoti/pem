package com.example.pem.views.event_overview

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Criteria
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.widget.SearchView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.MutableLiveData
import com.example.pem.R
import com.example.pem.addFragment
import com.example.pem.replaceFragment
import com.example.pem.views.SettingsActivity
import com.example.pem.views.event_overview.list.ListFragment
import com.example.pem.views.event_overview.map.MapFragment
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.synthetic.main.activity_event_overview.*
import kotlinx.android.synthetic.main.fragment_search.*


class EventOverviewActivity : androidx.appcompat.app.AppCompatActivity()  {
    private var locationManager: LocationManager? = null
    private var provider: String = ""
    private var currentLocation: LatLng? = null
    private var myLocation: MutableLiveData<LatLng> = MutableLiveData()

    private val containerId = R.id.containerEventOverview
    private var mapsVisible: Boolean = false

    private lateinit var listFragment: ListFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event_overview)

        requestLocationPermissions()
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager

        val criteria = Criteria()
        locationManager?.let { manager ->
            manager.getBestProvider(criteria, false).also {
                provider = it
            }
        }

        checkIfLocationIsEnabled()

        listFragment = ListFragment(currentLocation)
        addFragment(listFragment, containerId)
        buttonMap.setOnClickListener { switchFragment() }

        buttonSettings.setOnClickListener{
            val intent = Intent(this, SettingsActivity::class.java)
            this.startActivity(intent)
        }

        createEventFab.setOnClickListener { showCreateEvent() }

        searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener {

            override fun onQueryTextChange(p0: String?): Boolean {
                if (!mapsVisible) {
                    listFragment.listAdapter?.filter?.filter(p0)
                }

                return true
            }

            override fun onQueryTextSubmit(p0: String?): Boolean {
                return false
            }
        })
    }

    // Background

    private fun requestLocationPermissions() {
        val permissions = arrayOf(
            android.Manifest.permission.ACCESS_FINE_LOCATION,
            android.Manifest.permission.ACCESS_COARSE_LOCATION,
            android.Manifest.permission.READ_CALENDAR,
            android.Manifest.permission.WRITE_CALENDAR
        )
        ActivityCompat.requestPermissions(this, permissions,0)
    }

    private fun checkIfLocationIsEnabled() {
        val enabled = locationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)
        if (!enabled) {
            val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
            startActivity(intent)
        }
    }

    private val locationListener: LocationListener = object : LocationListener {
        override fun onLocationChanged(location: Location) {
            currentLocation = LatLng(location.latitude, location.longitude)
            myLocation.postValue(LatLng(location.latitude, location.longitude))
        }

        override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}

        override fun onProviderEnabled(provider: String) {
            Toast.makeText(applicationContext, "Enabled new provider $provider",
            Toast.LENGTH_SHORT).show()
        }

        override fun onProviderDisabled(provider: String) {
            Toast.makeText(applicationContext, "Disabled provider $provider",
                Toast.LENGTH_SHORT).show()
        }
    }

    override fun onResume() {
        super.onResume()
        if ( Build.VERSION.SDK_INT >= 23 &&
            ContextCompat.checkSelfPermission( this, android.Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED &&
            ContextCompat.checkSelfPermission( this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return
        }

        try {
            locationManager!!.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0L, 0f, locationListener)
        } catch (ex: SecurityException) {
            Log.d("EventOverviewActivity", "Security Exception, no location available")
        }
    }

    override fun onPause() {
        super.onPause()
        locationManager!!.removeUpdates(locationListener)
    }

    // UI

    private fun switchFragment() {
        if(mapsVisible) {
            listFragment = ListFragment(currentLocation)
            replaceFragment(listFragment, containerId)
        }
        else replaceFragment(MapFragment(myLocation), containerId)
        searchContainer.bringToFront()
        mapsVisible = !mapsVisible
    }

    private fun showCreateEvent() {
        val intent = Intent(this, CreateEventActivity::class.java)
        startActivity(intent)
    }
}
