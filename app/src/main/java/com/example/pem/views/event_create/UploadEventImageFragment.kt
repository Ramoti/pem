package com.example.pem.views.event_create

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.pem.R
import com.example.pem.interfaces.CreateEventButtonClickListener
import com.example.pem.repositories.EventRepository
import com.example.pem.repositories.UserRepository
import kotlinx.android.synthetic.main.fragment_upload_image.*


class UploadEventImageFragment(private var createEventButtonClickListener: CreateEventButtonClickListener): Fragment() {

    private val selectImageRequestCode = 1

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_upload_image, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        selectImageButton.setOnClickListener {
            startImagePicker()
        }

        uploadImageBackButton.setOnClickListener {
            createEventButtonClickListener.onBackClicked()
        }

        uploadImageContinueButton.setOnClickListener {
            createEventButtonClickListener.onContinueClicked()
        }

        UserRepository.currentUser.value?.let {user ->
            uploadImageTitle.text = "Hallo " + user.name
        }
    }

    private fun startImagePicker() {
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(intent, selectImageRequestCode)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == selectImageRequestCode && resultCode == RESULT_OK && null != data) {

                val selectedImage = data.data

                val bitmap = MediaStore.Images.Media.getBitmap(activity?.contentResolver, selectedImage)
                val resizedBitmap = Bitmap.createScaledBitmap(bitmap, 500, 500, false)

                selectImageView.setImageBitmap(resizedBitmap)
                EventRepository.eventImage = resizedBitmap
                uploadImageContinueButton.isEnabled = true
            }
    }
}