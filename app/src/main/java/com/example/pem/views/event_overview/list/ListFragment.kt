package com.example.pem.views.event_overview.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.pem.models.Event
import com.example.pem.viewModels.ListFragmentViewModel
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.synthetic.main.list_event_overview.*

class ListFragment(private val currentLocation: LatLng?) : Fragment() {

    lateinit var viewModel: ListFragmentViewModel

    var events: List<Event> = mutableListOf()

    var filterActive: Boolean = false

    var listAdapter: ListAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
        viewModel = ViewModelProviders.of(this).get(ListFragmentViewModel::class.java)
        viewModel.getEvents().observe(this, Observer<List<Event>>{ newEvents ->
            events = newEvents
            if (filterActive) {

            } else {
                initRecyclerList(events)
            }
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(com.example.pem.R.layout.list_event_overview, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    private fun initRecyclerList(eventList: List<Event>) {

        listAdapter = ListAdapter(eventList, context!!)
        eventRecyclerList.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = listAdapter
        }
    }

    companion object Filter {
        var filter: MutableLiveData<String> = MutableLiveData("")
    }
}