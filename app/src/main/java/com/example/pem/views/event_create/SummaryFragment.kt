package com.example.pem.views.event_create

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.pem.R
import com.example.pem.interfaces.CreateEventButtonClickListener
import com.example.pem.models.Location
import com.example.pem.repositories.UserRepository
import com.example.pem.viewModels.CreateEventViewModel
import kotlinx.android.synthetic.main.fragment_create_event_summary.*
import java.text.SimpleDateFormat
import java.util.*

class SummaryFragment(private val createEventViewModel: CreateEventViewModel, private val createEventButtonClickListener: CreateEventButtonClickListener) : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_create_event_summary,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        createEventViewModel.eventName.observe(this, Observer<String> { name ->
            summaryNameSubtitle.text = name
        })

        createEventViewModel.eventDescription.observe(this, Observer<String> { description ->
            summaryDescriptionSubtitle.text = description
        })

        createEventViewModel.eventDate.observe(this, Observer<Date> {date ->
            val dateFormat = SimpleDateFormat("dd-MM-yyyy")
            summaryDateSubtitle.text = dateFormat.format(date)
        })

        createEventViewModel.eventDate.observe(this, Observer<Date> {date ->
            val dateFormat = SimpleDateFormat("HH:mm")
            summaryTimeSubtitle.text = dateFormat.format(date)
        })

        createEventViewModel.eventLocation.observe(this, Observer<Location> { location ->
            summaryLocationSubtitle.text = location.name
        })

        backButton.setOnClickListener {
            createEventButtonClickListener.onBackClicked()
        }

        createEventButton.setOnClickListener {
            createEventButtonClickListener.createEventClicked()
        }

        UserRepository.currentUser.value?.let { user ->
            summaryTitle.text = "Hallo " + user.name
        }
    }
}