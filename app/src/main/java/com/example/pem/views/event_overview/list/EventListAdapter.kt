package com.example.pem.views.event_overview.list

import android.content.Context
import android.content.Intent
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.example.pem.R
import com.example.pem.inflate
import com.example.pem.models.Event
import com.example.pem.repositories.ImageRepository
import com.example.pem.repositories.UserRepository
import com.example.pem.views.event_details.EventDetailActivity
import kotlinx.android.synthetic.main.item_list_event_overview.view.*
import java.text.SimpleDateFormat

class ListAdapter(private val eventList: List<Event>, private val mContext: Context) : RecyclerView.Adapter<EventHolder>(), Filterable {

    private var listFiltered: MutableList<Event> = eventList as MutableList<Event>

    private lateinit var view: View

    override fun getFilter(): Filter {

        return object : Filter() {
            override fun publishResults(p0: CharSequence?, p1: FilterResults?) {
                p1?.let {
                    listFiltered = it.values as MutableList<Event>
                    notifyDataSetChanged()
                }
            }

            override fun performFiltering(p0: CharSequence?): FilterResults {
                var charString: String = p0.toString()
                if (charString.isEmpty()) {
                    listFiltered = eventList as MutableList<Event>
                } else {
                    var filteredList: MutableList<Event> = mutableListOf()
                    for (e: Event in eventList) {
                        if (e.title?.toLowerCase()!!.contains(charString.toLowerCase())) {
                            filteredList.add(e)
                        }
                    }

                    listFiltered = filteredList
                }

                var filterResults = FilterResults()
                filterResults.values = listFiltered
                return filterResults
            }
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventHolder {
        val inflatedView = parent.inflate(R.layout.item_list_event_overview, false)
        view = inflatedView
        return EventHolder(inflatedView)
    }

    override fun getItemCount() = listFiltered.size

    override fun onBindViewHolder(holder: EventHolder, position: Int) {
        val event = listFiltered[position]
        holder.bindItem(event)
    }

}

class EventHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {
    private var view: View = v
    private var event: Event? = null

    init {
        v.findViewById<CardView>(R.id.materialCardView).setOnClickListener(this)
    }

    override fun onClick(v: View) {
        val context = v.context
        val intent = Intent(context, EventDetailActivity::class.java)
        intent.putExtra("event", event)
        context.startActivity(intent)
    }

    fun bindItem(event:Event) {
        this.event = event
        view.eventOverviewTitle.text = event.title
        view.eventOverviewDescription.text = event.description

        event.time.let {
            val dateFormat = SimpleDateFormat("dd-MM-yyyy")
            view.eventOverviewDate.text = dateFormat.format(it)
        }

        if(event.members.containsKey(UserRepository.currentUser.value?.id)) {
            view.eventOverviewCheckmark.visibility = View.VISIBLE
        } else {
            view.eventOverviewCheckmark.visibility = View.GONE
        }

        ImageRepository.loadEventImage(event.id, view.eventOverviewImage, view.context)
    }
}
