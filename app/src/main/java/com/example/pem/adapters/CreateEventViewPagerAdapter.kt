package com.example.pem.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.example.pem.interfaces.CreateEventButtonClickListener
import com.example.pem.viewModels.CreateEventViewModel
import com.example.pem.views.event_create.*


class CreateEventViewPagerAdapter(fm: FragmentManager, private val createEventViewModel: CreateEventViewModel, private val buttonClickListener: CreateEventButtonClickListener): FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment {
        var fragment = Fragment()
        when (position) {
            0 -> fragment = EditNameAndDescriptionFragment(createEventViewModel, buttonClickListener)
            1 -> fragment = EditDateFragment(createEventViewModel, buttonClickListener)
            2 -> fragment = EditLocationFragment(createEventViewModel, buttonClickListener)
            3 -> fragment = UploadEventImageFragment(buttonClickListener)
            4 -> fragment = SummaryFragment(createEventViewModel, buttonClickListener)
        }

        return fragment
    }

    override fun getCount(): Int {
       return 5
    }

}