package com.example.pem.interfaces

interface CreateEventButtonClickListener {

    fun onContinueClicked()
    fun onBackClicked()
    fun createEventClicked()
}