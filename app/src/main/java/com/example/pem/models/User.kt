package com.example.pem.models

import android.graphics.Bitmap

class User {
    var id: String = ""
        get() = field
        set(value) {
            field = value
        }
    var name: String = ""
        get() = field
        set(value) {
            field = value
        }
    var email: String = ""
        get() = field
        set(value) {
            field = value
        }
    var phone: String? = ""
        get() = field
        set(value) {
            field = value
        }
    var events: MutableMap<String, Boolean> = mutableMapOf()
    var profilePicture: Bitmap? = null
}