package com.example.pem.models

import android.graphics.Bitmap
import android.os.Parcelable
import com.google.firebase.database.IgnoreExtraProperties
import kotlinx.android.parcel.Parcelize
import java.util.*


@IgnoreExtraProperties
@Parcelize
data class Event(
    var id: String = "",
    var title: String? = "",
    var description: String? = "",
    var creatorID: String = "",
    var members: MutableMap<String, Boolean> = mutableMapOf(),
    var location: com.example.pem.models.Location? = null,
    var time: Date? = Date(),
    var image: Bitmap? = null,
    var imageId: Int = 0
//    var calendarEventId: Long?
) : Parcelable