package com.example.pem.models

import android.os.Parcelable
import com.google.firebase.database.IgnoreExtraProperties
import kotlinx.android.parcel.Parcelize

@IgnoreExtraProperties
@Parcelize
data class Location (
    var name: String? = "",
    var lng: Double? = 0.0,
    var lat: Double? = 0.0,
    var address: String? = ""
) : Parcelable