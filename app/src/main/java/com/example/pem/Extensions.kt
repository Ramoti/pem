package com.example.pem

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.example.pem.models.Event
import com.example.pem.models.User
import java.io.IOException
import java.util.*

// RecyclerList

fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}

// Fragments

inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> FragmentTransaction) {
    beginTransaction().func().commit()
}

fun AppCompatActivity.addFragment(fragment: Fragment, frameId: Int){
    supportFragmentManager.inTransaction { add(frameId, fragment) }
}


fun AppCompatActivity.replaceFragment(fragment: Fragment, frameId: Int) {
    supportFragmentManager.inTransaction{replace(frameId, fragment)}
}

fun Event.containsSearchQuery(query: String): Boolean {
    if(query == "") return true
    this.title?.let {
        return it.contains(query, true)
    }
    this.description?.let {
        return it.contains(query, true)
    }
    return false
}

fun Event.assetsToBitmap(id: Int, context: Context): Bitmap?{
    return toBitmap(id, context)
}

fun User.assetsToBitmap(id: Int, context: Context): Bitmap? {
    return toBitmap(id, context)
}

private fun toBitmap(id: Int, context: Context): Bitmap? {
    return try{
        BitmapFactory.decodeResource(context.resources, id)
    }catch (e: IOException){
        e.printStackTrace()
        null
    }
}

fun Date.displayAsString(): String {
    return this.toString()
}